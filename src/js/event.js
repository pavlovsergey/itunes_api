'use strict'
var builder = new ListBuilder();
console.log(builder);
var form =  document.querySelector('.search__container');
var flag = true;

// event input	
form.addEventListener('submit', (e) => {
	e.preventDefault();
	
	builder.keyWords = document.querySelector('.search__input').value.replace(/\W/g,"+");
	
	builder.getContent().then( 
	(content) => {
			if (flag == true){
				flag = false;
				builder.createList(content)}
				flag = true;}
			,				
	(message) => { 
				let duration = 500;
				$('.list').fadeOut(duration) ; 
				document.querySelector('.message').innerHTML = message +"";	
				$('.message').fadeIn(duration);
				});
		
});

var globalplayer = require('./globalplayer'); 

document.querySelector('.list').addEventListener('click', function(e){

//open more information	 	
let View = require('./View'); 	

if (e.target.classList.contains('img_open')) {
		e.stopPropagation();
		if (flag == true) {
				flag = false;
				let item = $(e.target).closest('.list__item');
				let duration =500;
				if (!item.hasClass('.item__information_active')) {
					View.toggleOther($(e.target), item, duration)
					.then(() => {flag = true; console.log('есть')});
				}
				 
				else {
					View.toggleSelf($(e.target), item, duration)
					.then(() => {flag = true;  console.log('есть')});
				}
		}
	}

//globalplayer - music	
//globalplayer play/pause
	if (e.target.classList.contains('player__btn')) {
		e.stopPropagation();
		
		if (e.target === globalplayer.playingItem){
			globalplayer.playSelf();
		}
		else {
			globalplayer.playOther(e.target);
		}
	}

//globalplayer moveProgress
	if (e.target.classList.contains('indicator') || e.target.classList.contains('player__progres')) {
		let $this;
		e.target.classList.contains('player__progres') ? 
			$this = e.target : $this = e.target.closest('.player__progres');

	if (!globalplayer.playingItem) 
		{globalplayer.playingItem = $this.closest('.player')
									.querySelector('.player__btn');}
	if ($this.closest('.player').querySelector('.player__btn') === globalplayer.playingItem) {		
		globalplayer.moveProgress(e.clientX, $this);
		
	}
	}
},true);
		
//globalplayer.addEventListener('timeupdate',onProgress);	
globalplayer.addEventListener('timeupdate',globalplayer.onProgress);		

globalplayer.addEventListener('ended',globalplayer.goStart);	

