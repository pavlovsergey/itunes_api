'use strict'
class ListBuilder {
	
constructor (vcountResult , vqueryKind) {
	if (!vcountResult) {this.countResult = '&limit=30';} 
	else {this.countResult = '&limit=' + vcountResult}  //количество выводимых результатов max=200}

	if (!vqueryKind) {this.queryKind = '&entity=song'}
	else {this.queryKind = '&entity='+ vqueryKind} // вид контента согласно задания песни(song)	
	
	this.keyWords = document.querySelector('.search__input').value.replace(/\W/g,"+");
	this.List = document.querySelector('.list');	
}
	
getContent () {
	
	return new Promise ((resolve, reject) => {
		
		let xhr = new XMLHttpRequest();
	
		xhr.open('GET','https://itunes.apple.com/search?term='+ this.keyWords + this.queryKind + this.countResult); 
		xhr.addEventListener('load', () => {

			let content = JSON.parse(xhr.responseText);

			if (xhr.status == 200 && content.resultCount > 0) { resolve(content);}
			else if (content.resultCount == 0) { 
				 reject('По запросу ничего не найдено. Попробуйте переформулировать запрос')
				}
			else { reject('Извините результатов нет по техническим причинам')};	
		});	
		xhr.send();		
	});
}
	

createList (content) {	
	let duration = 500;
	$('.list__item:first')			
		.removeClass('.item__information_active')
		.find('.item__information')		
		.hide();

	$('.img_open:first')
		.attr('src', "/img/plus.png")
		.attr('alt', "More")
		.show();

	$('.imgcoll:first').css('opacity', '1').hide();
	
	$('.img-preloder:first').show();
	
	$('.player__btn:first').attr('data-time','0');
	$('.indicator:first').css('width','0');
	$('.player__time:first').text('00:30 min');
	let clone = this.List.querySelector('.list__item').cloneNode(true);

	let removeItem = this.List.querySelectorAll('.list__item');
	for (let i = 0; i < removeItem.length; i++) {
		this.List.removeChild(removeItem[i]); 
	}
	
	$('.list').fadeOut(duration);
	$('.message').fadeOut(duration);
	
	for ( let i = 0; content.resultCount > i; i++) {
	
		let itemList = clone.cloneNode(true);
		
		itemList.querySelector('.imgcoll').setAttribute('src', content.results[i].artworkUrl100); 
		itemList.querySelector('.item__artist').innerHTML = content.results[i].artistName;
		itemList.querySelector('.item__track').innerHTML = content.results[i].trackName;
		itemList.querySelector('.item__collection').innerHTML = content.results[i].collectionName;
		itemList.querySelector('.item__genre').innerHTML = content.results[i].primaryGenreName;
		

		itemList.querySelector('.player__btn').setAttribute('data-src',content.results[i].previewUrl);


		itemList.querySelector('.information__head').innerHTML = content.results[i].artistName +
		 "-" + content.results[i].trackName;
		let collection = itemList.querySelectorAll('.collection__value');
		collection[0].innerHTML = content.results[i].collectionName;
		collection[1].innerHTML = content.results[i].trackCount;
		collection[2].innerHTML = content.results[i].collectionPrice + " " + content.results[i].currency;
		let track = itemList.querySelectorAll('.track__value');
		track[0].innerHTML = getMin(content.results[i].trackTimeMillis);
		track[1].innerHTML = content.results[i].trackPrice + content.results[i].currency;
	
		this.List.appendChild(itemList);

	}

	$('.list').fadeIn(duration);


//preloader
	$('.imgcoll').on('load', function(e){ 
		$(this).fadeIn(duration);
		$(this).siblings().hide();					
	});



//function translate millisecond in minute
	function getMin (TimeMillis) {
			if (Math.round(TimeMillis%60000/1000)<10){ 
				return Math.floor(TimeMillis/60000) + ':0' + Math.round(TimeMillis%60000/1000) + 'min';}
			else {
				return Math.floor(TimeMillis/60000) +':' + Math.round(TimeMillis%60000/1000) + 'min';
			};
	}	
}	
}
