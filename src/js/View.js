module.exports = {
	toggleOther: function ($this, item, duration) {
		return new Promise ((resolve, reject) => {
		item
			.addClass('.item__information_active')
			.siblings()
			.removeClass('.item__information_active')
			.find('.item__information')
			.slideUp(duration);
			
		item
			.siblings()
			.find('.img_open')
			.attr('src', "/img/plus.png")
			.attr('alt', "More")
			.fadeIn(duration);
			
		$this
			.attr('src', "/img/minus.png")
			.attr('alt', "Roll UP")
			.fadeIn(duration);
			
		item
			.find('.item__information')
			.slideDown(duration, function () {
					resolve();	//flag = true;
				});
		});	
	},
	toggleSelf: function ($this, item, duration) {
		return new Promise ((resolve) => {	
		$this
			.attr('src', "/img/plus.png")
			.attr('alt', "More")
			.fadeIn(duration);

		item
			.removeClass('.item__information_active')
			.find('.item__information')
			.slideUp(duration, function() {
				resolve();//flag = true;
			});
		});
	}
};