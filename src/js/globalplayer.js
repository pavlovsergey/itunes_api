
var globalplayer = document.createElement('audio');
	globalplayer.playingItem;

	
	globalplayer.moveProgress = function (clientX, progressItem) {
		let widthClick = clientX - progressItem.clientLeft - progressItem.getBoundingClientRect().left;
		let progress = widthClick/progressItem.clientWidth;
		let timeSong = Math.round(this.duration*progress);
		this.currentTime = timeSong;
};

globalplayer.onProgress = function () {
	
	let progressBar = this.playingItem.closest('.player').querySelector('.indicator');
	let duration = this.duration;
	let currentTime = this.currentTime;
	let progress = Math.round(100/duration*currentTime);
	progressBar.style.width = progress + '%';

	if (Math.round(currentTime) < 10) {currentTime = '00:0'+ Math.round(currentTime) + 'min'} 
	else {currentTime = '00:'+ Math.round(currentTime) + 'min'} 
	let parent = this.playingItem.closest('.player');
	parent.querySelector('.player__time').innerHTML = currentTime;
};

globalplayer.goStart =  function () {
	this.playingItem.setAttribute("data-time",'0');
	this.playingItem.closest('.player').querySelector('.indicator').style.width = '0';
	this.playingItem.classList.remove('player__btn_active');
}
globalplayer.playSelf =  function (song) {

			if (this.paused){
				this.playingItem.classList.add('player__btn_active');
				this.play();
			}
			else {this.pause();
				  this.playingItem.classList.remove('player__btn_active');
			}
}
globalplayer.playOther =  function (song) {
			
			if (!this.playingItem) {this.playingItem = song;}
			if (!this.paused){
				console.log (this.playingItem);
				 this.playingItem.classList.remove('player__btn_active');
				}
			
			this.playingItem.setAttribute("data-time",this.currentTime);

			this.src = song.getAttribute("data-src");
			this.currentTime = song.getAttribute("data-time");
			this.playingItem = song;
			this.playingItem.classList.add('player__btn_active');
			this.play();
}
module.exports = globalplayer;
